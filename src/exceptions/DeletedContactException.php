<?php

namespace nuffic\getresponse\exceptions;

use yii\base\Exception;

/**
 * Class DeletedContactException
 * @package nuffic\getresponse\exceptions
 */
class DeletedContactException extends Exception
{
    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Deleted Contact';
    }
}
