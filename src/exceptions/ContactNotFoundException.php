<?php

namespace nuffic\getresponse\exceptions;

use yii\base\Exception;

/**
 * Class ContactNotFoundException
 * @package nuffic\getresponse\exceptions
 */
class ContactNotFoundException extends Exception
{
    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Contact not found';
    }
}
