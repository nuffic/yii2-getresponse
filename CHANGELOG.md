Under development
-----------------

1.6.0 2023-08-18
-----------------
- Feature: Upgrade guzzle 7 (mikk150)

1.5.0 2021-03-31
-----------------
- [TC2-969] Added BlockedContactException (Ilya Loginov)

1.4.0 2020-01-09
-----------------
- [TC2-756] Added DeletedContactException (Ilya Loginov)

1.3.0 2019-01-30
-----------------
- [IR-274] Added methods: `searchContactInAllLists` and `deleteContact` (Ilya)

1.2.0 2018-11-14
-----------------
- Add possibility to set domain (for enterprise users) (Kaupo Juhkam)

1.1.0 2018-11-05
-----------------
- Added BlacklistedContactException (Ilya Loginov)

1.0.0 2018-09-28
-----------------
- Initial implementation (Kaupo Juhkam)
