<?php

namespace tests\unit;

use Codeception\Stub;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Psr7\Response;
use nuffic\getresponse\Client;
use nuffic\getresponse\exceptions\BlockedContactException;
use nuffic\getresponse\exceptions\ClientException;
use nuffic\getresponse\exceptions\ContactNotFoundException;
use nuffic\getresponse\exceptions\DeletedContactException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\StreamInterface;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class ClientTest
 * @package tests\unit
 */
class ClientTest extends \Codeception\Test\Unit
{
    /**
     * @test
     */
    public function testApiKeyRequired()
    {
        $this->expectException(InvalidConfigException::class);
        new Client();
    }

    /**
     * @test
     */
    public function testSuccessfulInstantiation()
    {
        new Client([
            'apiKey' => '123',
        ]);
    }

    /**
     * @test
     */
    public function testApiKeyAddedToClient()
    {
        $client = new Client([
            'apiKey' => '123',
        ]);

        $headers = $client->httpClient->getConfig('headers');
        $this->assertArrayHasKey('X-Auth-Token', $headers);
        $this->assertEquals('api-key 123', ArrayHelper::getValue($headers, 'X-Auth-Token'));
    }

    /**
     * @test
     */
    public function testSearchContact()
    {
        $client = new Client([
            'apiKey' => '123',
            'httpClient' => Stub::make(HttpClient::class, [
                'request' => Stub\Expected::once(function ($method, $url, $params) {
                    $this->assertEquals('contacts?query[email]=email@example.org&fields=email', $url);

                    return new Response();
                }),
            ], $this),
        ]);
        $client->searchContact('email@example.org');
    }

    /**
     * @test
     */
    public function testSearchContactInAllLists()
    {
        $client = new Client([
            'apiKey' => '123',
            'httpClient' => Stub::make(HttpClient::class, [
                'request' => Stub\Expected::once(function ($method, $url, $params) {
                    $this->assertEquals('contacts?query[email]=email@example.org&fields=email', $url);
                    return Stub::make(Response::class, [
                        'getBody' => Stub::makeEmpty(StreamInterface::class, [
                            'getContents' => function () {
                                return Json::encode([
                                    [
                                        'email' => 'test@gmail.com',
                                        'contactId' => '3sdff'
                                    ],
                                    [
                                        'email' => 'test2@gmail.com',
                                        'contactId' => '4sdff'
                                    ],
                                ]);
                            }
                        ])
                    ]);
                }),
            ], $this),
        ]);
        $this->assertEquals(['3sdff', '4sdff'], $client->searchContactInAllLists('email@example.org'));
    }

    /**
     * @test
     */
    public function testAddContact()
    {
        $client = new Client([
            'apiKey' => '123',
            'httpClient' => Stub::make(HttpClient::class, [
                'request' => Stub\Expected::once(function ($method, $url, $params) {
                    $this->assertEquals('contacts', $url);
                    $this->assertEquals('email@example.org', ArrayHelper::getValue($params, 'json.email'));
                    $this->assertEquals('hurrdurr', ArrayHelper::getValue($params, 'json.campaign.campaignId'));
                    $this->assertEquals('param1', ArrayHelper::getValue($params, 'json.customFieldValues.0.customFieldId'));
                    $this->assertEquals('oh yes', ArrayHelper::getValue($params, 'json.customFieldValues.0.value.0'));

                    return new Response();
                }),
            ], $this),
        ]);
        $client->addContact('hurrdurr', 'email@example.org', ['param1' => 'oh yes']);
    }

    /**
     * @test
     */
    public function testUpdateCustomFields()
    {
        $client = new Client([
            'apiKey' => '123',
            'httpClient' => Stub::make(HttpClient::class, [
                'request' => Stub\Expected::once(function ($method, $url, $params) {
                    $this->assertEquals('contacts/asdfg/custom-fields', $url);
                    $this->assertEquals('param1', ArrayHelper::getValue($params, 'json.customFieldValues.0.customFieldId'));

                    return new Response();
                }),
            ], $this),
        ]);
        $client->updateCustomFields('asdfg', ['param1' => 'oh yes']);
    }

    /**
     * @test
     */
    public function testCustomFieldsMapperExcludesNullValue()
    {
        $client = new Client([
            'apiKey' => '123',
            'httpClient' => Stub::make(HttpClient::class, [
                'request' => Stub\Expected::once(function ($method, $url, $params) {
                    $this->assertEquals('contacts/asdfg/custom-fields', $url);
                    $this->assertEmpty(ArrayHelper::getValue($params, 'json.customFieldValues'));

                    return new Response();
                }),
            ], $this),
        ]);
        $client->updateCustomFields('asdfg', ['param1' => null]);
    }

    /**
     * @test
     */
    public function testDomainOption()
    {
        $client = new Client([
            'apiKey' => '123',
            'domain' => 'look at me, I\'m a hooman',
        ]);

        $this->assertEquals('look at me, I\'m a hooman', ArrayHelper::getValue($client->httpClient->getConfig('headers'), 'X-Domain'));
    }

    /**
     * @test
     */
    public function testNoHeaderWhenDomainNotSet()
    {
        $client = new Client([
            'apiKey' => '123',
        ]);

        $this->assertNull(ArrayHelper::getValue($client->httpClient->getConfig('headers'), 'X-Domain'));
    }

    /**
     * @test
     */
    public function testDeleteContactTrue()
    {
        $client = new Client([
            'apiKey' => '123',
            'httpClient' => Stub::make(HttpClient::class, [
                'request' => Stub\Expected::once(function ($method, $url, $params) {
                    return Stub::make(Response::class, [
                        'getStatusCode' => 204
                    ]);
                }),
            ], $this),
        ]);
        $this->assertTrue($client->deleteContact('test'));
    }

    /**
     * @test
     */
    public function testDeleteContactFalse()
    {
        $client = new Client([
            'apiKey' => '123',
            'httpClient' => Stub::make(HttpClient::class, [
                'request' => Stub\Expected::once(function ($method, $url, $params) {
                    return Stub::make(Response::class, [
                        'getStatusCode' => 422
                    ]);
                }),
            ], $this),
        ]);
        $this->assertFalse($client->deleteContact('test'));
    }

    /**
     * @test
     */
    public function testDeleteContactThrowsException()
    {
        $client = new Client([
            'apiKey' => '123',
            'httpClient' => Stub::make(HttpClient::class, [
                'request' => Stub\Expected::once(function ($method, $url, $params) {
                    $response = Stub::make(Response::class, [
                        'getStatusCode' => 200,
                        'getBody' => Stub::makeEmpty(StreamInterface::class, [
                            'getContents' => '{}'
                        ])
                    ]);
                    throw new BadResponseException('test', Stub::makeEmpty(RequestInterface::class), $response);
                }),
            ], $this),
        ]);
        $this->expectException(ClientException::class);
        $client->deleteContact('test');
    }

    /**
     * @test
     */
    public function testDeleteContactThrowsNotFoundException()
    {
        $client = new Client([
            'apiKey' => '123',
            'httpClient' => Stub::make(HttpClient::class, [
                'request' => Stub\Expected::once(function ($method, $url, $params) {
                    $response = Stub::make(Response::class, [
                        'getStatusCode' => 200,
                        'getBody' => Stub::makeEmpty(StreamInterface::class, [
                            'getContents' => Json::encode([
                                'message' => Client::CONTACT_NOT_FOUND_MESSAGE
                            ])
                        ])
                    ]);
                    throw new BadResponseException('test', Stub::makeEmpty(RequestInterface::class), $response);
                }),
            ], $this),
        ]);
        $this->expectException(ContactNotFoundException::class);
        $client->deleteContact('test');
    }

    /**
     * @test
     */
    public function testUnsubscribedContactThrowsNeededException()
    {
        $client = new Client([
            'apiKey' => '123',
            'httpClient' => Stub::make(HttpClient::class, [
                'request' => Stub\Expected::once(function ($method, $url, $params) {
                    $response = Stub::make(Response::class, [
                        'getStatusCode' => 200,
                        'getBody' => Stub::makeEmpty(StreamInterface::class, [
                            'getContents' => Json::encode([
                                'message' => Client::DELETED_CONTACT_MESSAGE
                            ])
                        ])
                    ]);
                    throw new BadResponseException('test', Stub::makeEmpty(RequestInterface::class), $response);
                }),
            ], $this),
        ]);
        $this->expectException(DeletedContactException::class);
        $client->addContact('test', 'test@gmail.com');
    }

    /**
     * @test
     */
    public function testAddContactThrowsBlockedContactException()
    {
        $client = new Client([
            'apiKey' => '123',
            'httpClient' => Stub::make(HttpClient::class, [
                'request' => Stub\Expected::once(function ($method, $url, $params) {
                    $response = Stub::make(Response::class, [
                        'getStatusCode' => 200,
                        'getBody' => Stub::makeEmpty(StreamInterface::class, [
                            'getContents' => Json::encode([
                                'message' => Client::BLOCKED_CONTACT_MESSAGE
                            ])
                        ])
                    ]);
                    throw new BadResponseException('test', Stub::makeEmpty(RequestInterface::class), $response);
                }),
            ], $this),
        ]);
        $this->expectException(BlockedContactException::class);
        $client->addContact('test', 'test@gmail.com');
    }
}
